from django.urls import path
from rest_framework import routers, serializers, viewsets
from . import views

urlpatterns = [
    path('sign_up/', views.sign_up, name='sign_up'),
    path('delete_user/', views.delete_user, name='delete_user'),
    path('user_login/', views.user_login, name='user_login'),
]

