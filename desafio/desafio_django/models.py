from django.db import models

class User(models.Model):
    name = models.CharField(max_length=30)
    email = models.CharField(max_length=30, unique=True)
    password = models.CharField(max_length=30)
    create_date = models.DateField()
    update_date = models.DateField()
    last_login_date = models.DateField()
    auth_token = models.CharField(max_length=200)

    # Sera um relacional
    # phones = models.CharField(max_length=80)
    def __str__(self):
    	return self.name


class UserPhones(models.Model):
	user_id = models.ForeignKey(User, on_delete=models.CASCADE)
	number = models.CharField(max_length=9)
	number_prefix = models.CharField(max_length=2)

	def __str__(self):
		return "(%s) %s" % (self.number_prefix, self.number)