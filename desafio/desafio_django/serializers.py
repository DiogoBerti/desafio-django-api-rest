from rest_framework import serializers
from desafio_django.models import User, UserPhones

class UserSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(required=True, allow_blank=True, max_length=50)
    email = serializers.CharField(required=True, allow_blank=True, max_length=50)
   	phones = serializers.ListField(serializers.CharField(max_length=50))

   	def create(self, validated_data):
        """
        Create and return a new `User` instance, given the validated data.
        """
        return User.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `User` instance, given the validated data.
        """
        instance.name = validated_data.get('name', instance.name)
        instance.email = validated_data.get('email', instance.email)
        instance.phones = validated_data.get('phones', instance.phones)
        instance.save()
        return instance