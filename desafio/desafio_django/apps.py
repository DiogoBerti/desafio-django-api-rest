from django.apps import AppConfig


class DesafioDjangoConfig(AppConfig):
    name = 'desafio_django'
