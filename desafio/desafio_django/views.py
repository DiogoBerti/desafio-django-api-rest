from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from desafio_django.models import User, UserPhones
import jwt,json
from datetime import date

# Elimina a necessidade de usar o CSRF Token...
@csrf_exempt
def sign_up(request):
    '''
        Recebe um Json com nome de Usuario, email e Telefones.
        Cria um registro no banco e retorna um json para o chamador...
    '''
    if request.method == "POST":
        data = json.loads(request.body)     
        payload = {
            'email': data['email'],
            'password': data['password']
        }
        
        jwt_token = {
            'token': jwt.encode(payload, "SECRET_KEY")
        }

        user_new = User(
            name=data['name'], 
            email=data['email'],
            password=data['password'],
            create_date=date.today(),
            update_date=date.today(),
            last_login_date=date.today(),
            auth_token=jwt_token.get('token').decode('utf-8')
        )


        user_new.save()
        for phone in data.get('phones'):
            phone_new = UserPhones(
                            number=phone.get('number'),
                            number_prefix=phone.get('number_prefix'),
                            user_id=user_new
                        )

            phone_new.save()
        return JsonResponse({
            'status':'Usuario %s criado' % user_new.name,
            'id': user_new.id,
            'email': user_new.email,
            'token': user_new.auth_token,
            'create_date':user_new.create_date
        }, content_type='application/json', safe=False)

    else:
        return_data = User.objects.all()
        return_phones = UserPhones.objects.all()
        final_data = []
        counter = 0
        for item in return_data:
            final_data.append({
                        'name':item.name,
                        'email':item.email,
                        'phones':[],
                        'id': item.id,
                        'token': item.auth_token
                    })
            for n in return_phones:
                if n.user_id.id == item.id:
                    final_data[counter].get('phones').append(
                        {
                            'number': n.number,
                            'number_prefix': n.number_prefix
                        }
                    )
            counter += 1
        return JsonResponse(final_data, content_type='application/json', safe=False)

@csrf_exempt
def delete_user(request, **kwargs):
    '''
    Recebe no Post o e-mail e token do usuario para poder executar a exclusão do usuario cujo Id 
    segue na url
    '''
    data = json.loads(request.body)
    # Token
    user_token = request.META.get('HTTP_AUTHORIZATION')
    try:
        userId = data.get('user_id')
        login = User.objects.filter(email=data.get('email'))
        if login[0].auth_token == user_token:
            if not userId:
                return JsonResponse({'error':"An error ocurred"},status=500)
            if request.method == 'POST':
                deleted_user = User.objects.filter(id=userId).delete()
                print(deleted_user[0])
                if deleted_user[0] == 0:
                    return JsonResponse({'error':"Usuario não encontrado"},status=404)
                return JsonResponse({'message': 'Usuario Deletado'}, content_type='application/json', safe=False)
        else:
            # Token não reconhecido...
            return JsonResponse({'Error': "Forbidden"}, status="403")
    except User.DoesNotExist:
        return JsonResponse({'error':"Usuario não encontrado"},status=404)
    
@csrf_exempt
def user_login(request):
    if request.method == "POST":
        data = json.loads(request.body)     
        login = User.objects.get(email=data.get('email'))
        print(login)
        print(login.password)
        if login:
            if login.password == data.get('password'):
                payload = {
                    'email': data['email'],
                    'password': data['password']
                }
                
                jwt_token = {
                    'token': jwt.encode(payload, "SECRET_KEY")
                }
                login.last_login_date=date.today()
                login.auth_token=jwt_token.get('token').decode('utf-8')
                login.save()
                return JsonResponse({
                    'message': 'Login Realizado com sucesso',
                    'token':jwt_token.get('token').decode('utf-8')
                }, status="201")

            else:
                return JsonResponse({'Error': "Usuario e ou senha estão errados"}, status="401")
        else:
            return JsonResponse({'error':"Usuario não encontrado"},status=404)
    else:
        return JsonResponse({'error':"An error ocurred"},status=500)